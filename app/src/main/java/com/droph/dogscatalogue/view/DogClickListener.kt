package com.droph.dogscatalogue.view

import android.view.View

interface DogClickListener {
    fun onDogClicked(v: View)
}