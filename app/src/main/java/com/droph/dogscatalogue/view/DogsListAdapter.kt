package com.droph.dogscatalogue.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.droph.dogscatalogue.R
import com.droph.dogscatalogue.databinding.ItemDogBinding
import com.droph.dogscatalogue.model.DogBreed
import com.droph.dogscatalogue.util.getProgressDrawable
import com.droph.dogscatalogue.util.loadImage
import kotlinx.android.synthetic.main.item_dog.view.*

class DogsListAdapter(val dogsList: ArrayList<DogBreed>) : RecyclerView.Adapter<DogsListAdapter.DogViewHolder>() , DogClickListener{
    override fun onDogClicked(v: View) {
        val uuid = v.dogId.text.toString().toInt()
        val action = ListFragmentDirections.actionDetailFragment()

        action.dogUuid = uuid
        Navigation.findNavController(v).navigate(action)
    }

    fun updateDogList(newDogsList: List<DogBreed>) {
        dogsList.clear()
        dogsList.addAll(newDogsList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        //val view = inflater.inflate(R.layout.item_dog, parent, false)
        val item = DataBindingUtil.inflate<ItemDogBinding>(inflater, R.layout.item_dog, parent, false)
        return DogViewHolder(item)
    }

    override fun getItemCount() = dogsList.size

    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {
        holder.item.dog = dogsList[position]
        holder.item.listener = this
}


    class DogViewHolder(var item: ItemDogBinding) : RecyclerView.ViewHolder(item.root)
}