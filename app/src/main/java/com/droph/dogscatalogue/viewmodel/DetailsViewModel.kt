package com.droph.dogscatalogue.viewmodel

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.droph.dogscatalogue.model.DogBreed
import com.droph.dogscatalogue.model.DogDatabase
import kotlinx.coroutines.launch

class DetailsViewModel(application: Application) : BaseViewModel(application) {
    val dogLiveData = MutableLiveData<DogBreed>()

    fun fetch(dogUuid: Int) {
        launch {
            dogLiveData.value = DogDatabase(getApplication()).dogDao().getDog(dogUuid)
        }
    }
}